package capaDomini;

import java.util.List;

public interface IRegistre {
	enum Nivell 
    { 
        Error(4),Avis(3),Informacio(2),Debug (1),Trassa(0) ;
		
        int nivell;
		   Nivell(int n) {
		      nivell = n;
		   }
		   int getNivell() {
		      return nivell;
		   }     
    } 
	
   public void estableixNivell(Nivell n) throws Exception;
   public String registra(Nivell n, String missatge) throws Exception;
   public void tanca() throws Exception;
   public List<String> registre()throws Exception;
   // el clear es perquè si els registres tenen un estàtic i no els netegem mai, l'ordre dels tests pot influir
   public void clear();
   	
}